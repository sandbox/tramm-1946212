<?php

/**
 * @file
 * Parses data from Google API.
 */

/**
 * Parses data from an Google Calendar.
 */
class FeedsGCalParser extends FeedsParser {

  /**
   * Implements FeedsParser::parse().
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {

    $result = new FeedsParserResult();
    foreach ($fetcher_result->getRaw() as $event) {
      foreach ($event->when as $when) {
        $item = array();
        $item["title"] = $event->title;
        $item["starttime"] = strtotime($when->startTime);
        $item["endtime"] = strtotime($when->endTime);
        $item["starttime_str"] = $when->startTime;
        $item["endtime_str"] = $when->endTime;
        $item["where"] = implode(" | ", $event->where);
        $item["link"] = $event->link[0]->href;
        $item["author_name"] = $event->author[0]->name;
        $item["author_email"] = $event->author[0]->email;
        $descr = (string) $event->content;
        $item["description"] = $descr;
        preg_match_all('#\bhttps?://[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/))#', $descr, $matches);
        $item["text_link"] = end($matches[0]);
        $item["id"] = $event->id . "_" . urlencode($when);
        $result->items[] = $item;
      }
    }

    return $result;
  }

  /**
   * Override parent::getMappingSources().
   */
  public function getMappingSources() {
    return array(
      'title' => array(
        'name' => t('Item title'),
        'description' => t('Title of the feed item.'),
      ),
      'id' => array(
        'name' => t('ID'),
        'description' => t('The feed item identificator.'),
      ),
      'link' => array(
        'name' => t('Link URL'),
        'description' => t('The link provided to event.'),
      ),
      'text_link' => array(
        'name' => t('Text link'),
        'description' => t('The last link provided in description text.'),
      ),
      'description' => array(
        'name' => t('Item description'),
        'description' => t('The contents or description of the event.'),
      ),
      'where' => array(
        'name' => t('Locations list'),
        'description' => t('Where does it take place?'),
      ),
      'author_email' => array(
        'name' => t('Author e-mail'),
        'description' => t('E-mail address of an author.'),
      ),
      'author_name' => array(
        'name' => t('Author name'),
        'description' => t('Name of an author.'),
      ),
      'starttime' => array(
        'name' => t('Start time'),
        'description' => t('When does it start?'),
      ),
      'endtime' => array(
        'name' => t('End time'),
        'description' => t('When does it finish?'),
      ),
      'starttime_str' => array(
        'name' => t('Start time str'),
        'description' => t('When does it start?'),
      ),
      'endtime_str' => array(
        'name' => t('End time str'),
        'description' => t('When does it finish?'),
      ),
    ) + parent::getMappingSources();
  }

}
