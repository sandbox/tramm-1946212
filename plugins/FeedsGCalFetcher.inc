<?php

/**
 * @file
 * Fetches data from Google API.
 */

/**
 * Fetches data from Google Calendar using Zend Framework.
 */
class FeedsGCalFetcher extends FeedsFetcher {

  /**
   * Implements FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    $user = trim($source_config['user']);
    $pass = $source_config['password'];

    $results = array();

    require_once 'Zend/Loader.php';
    Zend_Loader::loadClass('Zend_Gdata');
    Zend_Loader::loadClass('Zend_Gdata_ClientLogin');
    Zend_Loader::loadClass('Zend_Gdata_Calendar');
    Zend_Loader::loadClass('Zend_Date');

    $service = Zend_Gdata_Calendar::AUTH_SERVICE_NAME;
    $client = Zend_Gdata_ClientLogin::getHttpClient($user, $pass, $service);
    $service = new Zend_Gdata_Calendar($client);

    try {
      $calendar_feed = $service->getCalendarListFeed();
    }
    catch (Zend_Gdata_App_Exception $e) {
      watchdog('gcal', "Error: " . $e->getMessage());
    }

    foreach ($calendar_feed as $calendar) {
      $url = $calendar->link[0]->href;
      $query = $service->newEventQuery($url);
      $query->setUser(NULL);
      $query->setVisibility(NULL);
      $query->setProjection(NULL);
      $query->setOrderby('starttime');
      $query->setStartMin(date("c", strtotime('-1 month')));
      $query->setStartMax(date("c", strtotime('+12 month')));
      $event_feed = $service->getCalendarEventFeed($query);
      foreach ($event_feed as $event) {
        $results[] = $event;
      }
    }

    return new FeedsFetcherResult($results);
  }


  /**
   * Override parent::sourceForm().
   */
  public function sourceForm($source_config) {
    $form = array();
    $form['user'] = array(
      '#type' => 'textfield',
      '#title' => t('User'),
      '#description' => t('User name to log onto Google Calendar'),
      '#default_value' => isset($source_config['user']) ? $source_config['user'] : '',
      '#maxlength' => NULL,
      '#required' => TRUE,
    );
    $form['password'] = array(
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#description' => t("User's password for Google Calendar (don't use a valueable password!)"),
      '#default_value' => isset($source_config['password']) ? $source_config['password'] : '',
      '#maxlength' => NULL,
      '#required' => TRUE,
    );
    return $form;
  }

}
