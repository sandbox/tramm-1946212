FEEDS GCal
==========

Adds Google Calendar import options to the FEEDS import and aggregation
framework for Drupal. See http://drupal.org/project/feeds_gcal for
details.

The Google Calendar import depends on Zend Framework.
